<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170911075815 extends AbstractMigration {
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema){
        $this->addSql('CREATE TABLE winner (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, turns INT NOT NULL, created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE = InnoDB');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) {
      $this->addSql('DROP TABLE winner');
    }
}
