define(
  'applyAttributes',
  ['module', 'jquery'],
  function(module, $, undefined){
    "use strict";

    /**
     * Helper for transmitting arguments to components
     * @param params
     * @param attributes
     * @param values
     * @param defaultAttribute
     */
    return function (params, attributes, values, defaultAttribute) {
      if(typeof values === "string" && defaultAttribute && defaultAttribute !== ''){
        params[defaultAttribute] = values;
      }else if(typeof values === "object"){
        $.each(attributes, function (pos, name) {
          if(typeof values[name] !== "undefined"){
            params[name] = values[name];
          }
        });
      }
    };
  }
);
