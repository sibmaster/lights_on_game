define(
  'QueryLayer',
  ['module', 'jquery', './applyAttributes'],
  function(module, $, applyAttributes, undefined){
    "use strict";

    /**
     * Service for runtime process
     * It uses a connection service and provide an interface for a top-level commands)
     * @param inputConfig
     * @returns {{getAll: getAll, create: create, get: get, set: set, delete: delete}}
     * @constructor
     */
    return function (inputConfig) {
      var that = this;
      var kernel = {
        connection: null
      };
      applyAttributes(kernel, ['connection'], inputConfig, null);

      /**
       * runtime interface
       */
      return {
        action: function (data) {
          var result = $.Deferred();
          kernel.connection.query({
            //type: 'post',
            type: 'get',
            data: data,
            description: 'Making an action "' + data.action + '"'
          })
            .then(result.resolve, function () {
              console.error('QueryLayer.action() failed ', arguments);
            });
          return result.promise();
        }
      };
    };
  }
);
