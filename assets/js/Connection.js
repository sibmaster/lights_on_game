define(
  'Connection',
  ['module', 'jquery', './applyAttributes'],
  function(module, $, applyAttributes, undefined){
    "use strict";

    /**
     * Service providing connection and queueing requests
     * @param inputConfig
     * @returns {{query: query}}
     * @constructor
     */
    return function (inputConfig) {
      var that = this;
      var config = {
        url: null,
        onLogChange: function (queue, instance) {},
        logQueries: true
      };
      applyAttributes(config, ['url', 'onLogChange', 'logQueries'], inputConfig, 'url');

      /**
       * Promise based queue of pending requests
       */
      var promise = $.when();

      /**
       * Queue (history) of all requests
       * Contains past, current and pending requests
       * @type {Array}
       */
      var queue = [];

      /**
       * Adding to history (Queue)
       * @param item
       * @returns {Number}
       */
      var addQueueItem = function (item) {
        var index = queue.length;
        queue.push(item);
        config.onLogChange(queue, that);
        return index;
      };

      /**
       * Updating status of pending item
       * @param index
       * @param item
       */
      var updateQueueItem = function (index, item) {
        if(typeof queue[index] !== "undefined"){
          queue[index] = $.extend(queue[index], item);
          config.onLogChange(queue, that);
        }
      };

      return {
        /**
         * Interface of the service
         * @param params
         * @returns {*}
         */
        query: function ( params) {
          var index = 0;
          if(config.logQueries){
            var info = {
              description: '',
              status: 'pending'
            };
            applyAttributes(info, ['description'], params, '');
            if(info.description === ''){
              info.description = 'Unknown query ' + JSON.stringify(params).substr(0, 50) + '...';
            }
            var index = addQueueItem(info);
          }

          var result = $.Deferred();

          var query = {
            type: "GET",
            dataType: 'json',
            data: null,
            //data: JSON.stringify(data),
            //contentType: "application/json; charset=utf-8",
            url: config.url,
            success: function(responce){
              if(config.logQueries){
                updateQueueItem(index, {status: 'completed'});
              }
              result.resolve(responce);
            },
            error: function(responce){
              if(config.logQueries){
                updateQueueItem(index, {status: 'failed'});
              }
              //respect error handling
              result.reject(responce);
              //On errors we need to restart queue in order to continue working
              promise = $.when();
            }
          };
          applyAttributes(query, ['type', 'url', 'data', 'dataType'], params, 'url');
          // query.data = query.data !== null ? JSON.stringify(query.data) : null;

          //any request is always continuing queue
          promise = promise.then(function(){
            if(config.logQueries){
              updateQueueItem(index, {status: 'requesting'});
            }
            return $.ajax(query);
          });
          return result.promise();
        }
      };
    };
  }
);
