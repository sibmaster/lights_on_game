define(
  ['module', 'jquery', 'bootstrap', './LampPlotApp'],
  function(module, $, bootstrap, LampPlotApp, undefined){
    "use strict";

    var plot = $('.js--plot');
    var digitCount = $('.js--info-turns');
    var startBtn = $('.js--start');
    var undoBtn = $('.js--undo');
    var winnersBtn = $('.js--winners');

    function showModal(content) {
      var modal = $('#modalTemplate').clone();
      modal.find('.modal-content').append(content);
      modal.modal();
      modal.find('.js--action').click(function (e) {
        var action = $(this).data('action');

        makeAction(action, $(this), e).then(function (toClose) {
          if(toClose){
            modal.modal('hide');
          }
        });
      });
      return modal;
    }

    function makeAction(action, btn, e) {
      var result = $.Deferred();
      switch (action){
        case 'start':
          plot.LampPlotApp('start');
          result.resolve(true);
          break;
        case 'sendWinnerName':
          var name = btn.parents('.modal-content').find('input[name=name]').val();
          $.post({
            url: '/ajax/inputWinnerName',
            data: {
              name: name
            }
          }).then(function (data) {
            if(data.status){
              result.resolve(true);
              plot.LampPlotApp('blank');
              $.get('/winners').then(showModal);
            }
          });
          break;
      }
      return result.promise();
    }

    if(plot.length){
      plot.LampPlotApp({
        container: plot,
        buttonSelector: '.plot--cell',
        buttonOnClass: 'plot--cell-state-on',
        url: '/ajax/query',
        onUpdate: function (data) {
          digitCount.html(data.turns);
          console.log('data', data);
          if(data.status === 'win'){
            $.get('/inputWinnerName').then(showModal);
          }
        }
      });

      startBtn.click(function () {
        $.get('/new').then(showModal);
      });

      undoBtn.click(function () {
        plot.LampPlotApp('undo');
      });

      winnersBtn.click(function () {
        $.get('/winners').then(showModal);
      });
    }
  }
);
