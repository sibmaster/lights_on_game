define(
  'LampPlotApp',
  ['module', 'jquery', './applyAttributes', './QueryLayer', './Connection'],
  function(module, $, applyAttributes, QueryLayer, Connection, undefined){
    "use strict";

    var LampPlotApp = function () {
      var
        $canvas = $(arguments[0]),
        action = arguments.length > 1 ? arguments[1] : 'init',
        param = arguments.length > 2 ? arguments[2] : null;

      var instance = $canvas.data('LampPlotApp');

      if(typeof instance === 'undefined' || typeof instance === 'string' || instance === null){
        instance = new function () {
          var config = {
            container: null,
            buttonSelector: 'div',
            buttonOnClass: 'on',
            onUpdate: function () {},
            url: null
          };
          /**
           * Application components
           */
          var app = {
            /**
             * storage of game play data
             */
            data: {
              plot: []
            },
            /**
             * DOM node of the app
             */
            container: null,
            /**
             * DOM nodes of the buttons
             */
            buttons: null,
            /**
             * connection service
             */
            connection: null,
            /**
             * abstraction layer service
             */
            query: null,
            /**
             * UI component
             */
            ui: {
              setStateOn: function (el){
                if(!el.hasClass(config.buttonOnClass)){
                  el.addClass(config.buttonOnClass);
                }
              },
              setStateOff: function (el){
                if(el.hasClass(config.buttonOnClass)){
                  el.removeClass(config.buttonOnClass);
                }
              },
              updatePlot: function () {
                app.buttons.each(function () {
                  var index = $(this).data('index');
                  if (app.data.plot.indexOf(index) !== -1) {
                    app.ui.setStateOn($(this));
                  }else{
                    app.ui.setStateOff($(this));
                  }
                });
              },
              updateLog: function (queue) {},
              startEditingItem: function ($item) {},
              startCreatingItem: function () {},
            },
            /**
             * Dispatcher (main controller)
             */
            dispatcher: {
              blank: function () {
                setData({
                  data: {
                    plot: [],
                    turns: 0,
                    status: 'idle'
                  }
                });
              },
              processClick: function (el) {
                app.query
                  .action({
                    action: 'continue',
                    data: {
                      index: el.data('index')
                    }
                  })
                  .then(setData);
              },
              startNewGame: function () {
                app.query
                  .action({
                    action: 'start'
                  })
                  .then(setData);
              },
              undoLastStep: function () {
                app.query
                  .action({
                    action: 'undo'
                  })
                  .then(setData);
              }
            }
          };


          /**
           * Initialization of a widget
           * It is called on construction and finished if the app container is
           * set If the app container is not set the initialization is canceled
           * and can be called later with setting a container selector.
           */
          var init = function () {
            if(arguments.length){
              applyAttributes(config, ['container', 'buttonSelector', 'buttonOnClass', 'onUpdate', 'url'], arguments[0], 'container');
            }
            if(typeof config.container === "string") config.container = $(config.container);
            if(config.container instanceof jQuery && config.container.length === 1){
              app.container = config.container;
              app.buttons = app.container.find(config.buttonSelector);

              app.url = config.url ? config.url : app.container.data('url');

              app.buttons.click(function () {
                app.dispatcher.processClick($(this));
              });
            }


            app.connection = new Connection({
              url: app.url
            });

            app.query = new QueryLayer({
              connection: app.connection
            });

          };

          function setData(data) {
            app.data.plot = $.extend([], data.data.plot);
            app.data.turns = data.data.turns;
            app.data.status = data.data.status;
            app.ui.updatePlot();
            config.onUpdate($.extend({}, app.data));
          }

          return {
            init: init,
            start: app.dispatcher.startNewGame,
            undo: app.dispatcher.undoLastStep,
            blank: app.dispatcher.blank
          };
        };

        $canvas.data('LampPlotApp', instance);
      }

      return instance[action](param);
    };



    $.fn.LampPlotApp = function() {
      var action = 'init',
        result = null,
        param = null;

      if(arguments.length = 1){
        if(typeof arguments[0] === 'string'){
          action = arguments[0];
        }else{
          param = arguments[0];
        }
      }else if(arguments.length > 1){
        action = arguments[0];
        param = arguments[1];
      }

      $(this).each(function () {
        result = LampPlotApp($(this), action, param);
      });

      return result;
    };

    return LampPlotApp;
  }
);
