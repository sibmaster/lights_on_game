<?php

namespace AppBundle\Entity;

use Assetic\Exception\Exception;
use Doctrine\ORM\Mapping as ORM;

/**
 * Winner
 *
 * @ORM\Table(name="winner")
 * @ORM\Entity
 */
class Winner
{
  /**
   * @var string
   *
   * @ORM\Column(name="name", type="string", length=255, nullable=false)
   */
  private $name;

  /**
   * @var integer
   *
   * @ORM\Column(name="turns", type="integer", nullable=false)
   */
  private $turns;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="created_at", type="datetime", nullable=false)
   */
  private $createdAt = 'now()';

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="updated_at", type="datetime", nullable=false)
   */
  private $updatedAt = 'now()';

  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  public function __construct(array $attributes = array()){
    $this->createdAt = new \DateTime();
    $this->updatedAt = new \DateTime();
    foreach ($attributes as $key => $attribute){
      if(in_array($key, ['name', 'turns'])){
        $this->$key = $attribute;
      }
    }
  }

  public function getId(){
    return $this->id;
  }

  public function getName(){
    return $this->name;
  }

  public function getTurns(){
    return $this->turns;
  }

  public function getCreatedAt(){
    return date('d.m.Y H:i:s', $this->createdAt->getTimestamp());
  }

  public function getUpdatedAt(){
    return date('d.m.Y H:i:s', $this->updatedAt->getTimestamp());
  }

}

