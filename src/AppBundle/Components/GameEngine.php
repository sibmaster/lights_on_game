<?php

namespace AppBundle\Components;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class GameEngine {

  protected $width = 5;
  protected $height = 5;

  /**
   * @return array
   */
  protected function createGameData(){
    return array(
      'plot' => [],
      'history' => [],
      'status' => 'play'
    );
  }

  protected function getGoal(){
    return range(0, $this->width * $this->height - 1);
  }

  /**
   * @return array
   */
  protected function getGameData(){
    return !empty($_SESSION['gameData']) ? $_SESSION['gameData'] : $this->createGameData();
  }

  /**
   * @param array $gameData
   *
   * @return bool
   */
  protected function setGameData(array $gameData = array()){
    $_SESSION['gameData'] = $gameData;
    return true;
  }

  protected function toCoordinates($index){
    $x = $index % $this->width;
    $y = floor($index / $this->width);
    if(
      $x >= 0 && $x <  $this->width &&
      $y >= 0 && $y <  $this->height
    ){
      return [
        'x' => $x,
        'y' => $y
      ];
    }else{
      throw new BadRequestHttpException('The cell index is out of the plot');
    }
  }

  protected function toIndex($coordinates){
    if(
      isset($coordinates['x']) && isset($coordinates['y']) &&
      $coordinates['x'] >= 0 && $coordinates['x'] <  $this->width &&
      $coordinates['y'] >= 0 && $coordinates['y'] <  $this->height
    ){
      $x = (int) $coordinates['x'];
      $y = (int) $coordinates['y'];

      return $y * $this->height + $x;
    }else{
      throw new BadRequestHttpException('The cell index is out of the plot');
    }
  }

  /**
   * determines coordinates around
   *
   * @param $centerX
   * @param $centerY
   *
   * @return array
   */
  protected function getCoordinatesAround($centerX, $centerY){
    $centerX = (int) $centerX;
    $centerY = (int) $centerY;
    $coordinatesList = [];
    for($x = max($centerX - 1, 0); $x <= min($centerX + 1, $this->width - 1); $x++){
      for($y = max($centerY - 1, 0); $y <= min($centerY + 1, $this->height - 1); $y++){
        if(!($x === $centerX && $y === $centerY)){
          $coordinatesList[] = [
            'x' => $x,
            'y' => $y
          ];
        }
      }
    }
    return $coordinatesList;
  }

  /**
   * @param $gameData
   *
   * @return bool
   */
  protected function isWon($gameData){
    $goal = $this->getGoal();
    $rest = array_diff($goal, $gameData['plot']);
    return empty($rest);
  }

  /**
   * Game runtime process
   *
   * @param string $action
   * @param array $data
   *
   * @return array
   */
  public function handleAction($action = '', $data = []){
    $gameData = $this->getGameData();
    switch ($action){
      case 'refresh':
        break;
      case 'start':
        $gameData = $this->createGameData();
        break;
      case 'undo':
        if(count($gameData['history'])){
          $gameData['plot'] = array_pop($gameData['history']);
        }
        break;
      case 'continue':
      default:
        //current state of plot is already history
        $gameData['history'][] = $gameData['plot'];

        $centerIndex = (int) $data['index'];
        $centerCoordinates = $this->toCoordinates($centerIndex);
        $coordinatesAround = $this->getCoordinatesAround($centerCoordinates['x'], $centerCoordinates['y']);
        $indexesAround = [];
        foreach ($coordinatesAround as $coordinates){
          $indexesAround[] = $this->toIndex($coordinates);
        }
        $toSwitchCenterLamp = !in_array($centerIndex, $gameData['plot']);
        //determines what to switch on
        $toSwitch = array_diff($indexesAround, $gameData['plot']);
        //determines what to switch off
        $toHide = $indexesAround;

        if($toSwitchCenterLamp){
          $toSwitch[] = $centerIndex;
        }else{
          $toHide[] = $centerIndex;
        }

        //switching off lamps around
        $gameData['plot'] = array_diff($gameData['plot'], $toHide);
        //switching on lamps around
        $gameData['plot'] = array_merge($gameData['plot'], $toSwitch);

        if($this->isWon($gameData)){
          $gameData['status'] = 'win';
        }

        break;

    }
    $this->setGameData($gameData);
    $response = [
      'plot' => $gameData['plot'],
      'turns' => count($gameData['history']),
      'status' => $gameData['status']
    ];
    return  $response;
  }

  /**
   * @return array
   */
  public function getPlot(){
    $gameData = $this->getGameData();
    return $gameData['plot'];
  }

  /**
   * @return int
   */
  public function getStatus(){
    $gameData = $this->getGameData();
    return $gameData['status'];
  }

  /**
   * @return int
   */
  public function isGameWon(){
    $gameData = $this->getGameData();
    return $gameData['status'] === 'win';
  }

  /**
   * @return string
   */
  public function getTurns(){
    $gameData = $this->getGameData();
    return count($gameData['history']);
  }
}

