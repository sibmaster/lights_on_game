<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Components\GameEngine;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Entity\Winner;

class DefaultController extends Controller {
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request, GameEngine $gameEngine, Session $session){
      $session->start();
      $plot = $gameEngine->getPlot();
      return $this->render('default/index.html.twig', [
          'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
          'plot' => $plot
      ]);
    }

  /**
   * @Route("/ajax/query", name="ajax gate")
   */
  public function ajaxQueryAction(Request $request, GameEngine $gameEngine, Session $session){
    $session->start();
    $response = $gameEngine->handleAction(
      $request->query->get('action'),
      $request->query->get('data')
    );

    return new JsonResponse(array('data' => $response));
  }

  /**
   * @Route("/new", name="New Game Form")
   */
  public function newAction(Request $request, GameEngine $gameEngine, Session $session){
    $session->start();
    return $this->render('default/_partials/newGameForm.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
    ]);
  }

  /**
   * @Route("/inputWinnerName", name="Write Your Name")
   */
  public function inputWinnerNameAction(Request $request, GameEngine $gameEngine, Session $session){
    $session->start();

    $name = !empty($_SESSION['name']) ? $_SESSION['name'] : '';

    return $this->render('default/_partials/inputWinnerNameForm.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
      'name' => $name
    ]);
  }

  /**
   * @Route("/winners", name="Winners")
   */
  public function winnersAction(Request $request, GameEngine $gameEngine){
    $repository = $this->getDoctrine()->getRepository(Winner::class);

    $winners = $repository->findBy([], ['turns' => 'asc', 'createdAt' => 'asc'], 30);

    return $this->render('default/_partials/winners.html.twig', [
      'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
      'winners' => $winners
    ]);
  }

  /**
   * @Route("/ajax/inputWinnerName", name="ajax post from")
   */
  public function ajaxInputWinnerNameAction(Request $request, GameEngine $gameEngine, Session $session){
    $session->start();

    $status = 0;

    if($gameEngine->isGameWon()){
      $name = $request->get('name');

      $em = $this->getDoctrine()->getManager();

      $_SESSION['name'] = $name;

      $winner = new Winner([
        'name' => $name,
        'turns' => $gameEngine->getTurns()
      ]);

      $em->persist($winner);
      $em->flush();

      $gameEngine->handleAction('start');

      $status = 1;
    }

    return new JsonResponse(array('status' => $status));
  }


}

